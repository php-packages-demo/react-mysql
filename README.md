# react/[mysql](https://packagist.org/packages/react/mysql)

Async MySQL database client for ReactPHP

[![PHPPackages Rank](http://phppackages.org/p/react/mysql/badge/rank.svg)
](http://phppackages.org/p/react/mysql)
[![PHPPackages Referenced By](http://phppackages.org/p/react/mysql/badge/referenced-by.svg)
](http://phppackages.org/p/react/mysql)